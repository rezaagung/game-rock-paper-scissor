package com.company;
import java.util.Random;
import java.util.Scanner;

public abstract class Menu {
    String personPlay;
    String computerPlay = "";
    String ulang;
    String username;
    Integer win = 0,lose = 0, tie = 0;
    Integer computerInt;
    String response;
    public Menu(){};

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public Menu(String username){
        this.username=username;
    }

    Scanner scan = new Scanner(System.in);
    Random generator = new Random();
    public void play() {
        do {
            System.out.println("========================");
            System.out.println("Game Batu Gunting kertas");
            System.out.println("========================");
            computerInt = generator.nextInt(3) + 1;
            if (computerInt == 1)
                computerPlay = "R";
            else if (computerInt == 2)
                computerPlay = "P";
            else if (computerInt == 3)
                computerPlay = "S";
            System.out.println("R = Rock");
            System.out.println("S = Scrissor");
            System.out.println("P = Paper");
            System.out.print("Enter your choice : ");
            personPlay = scan.next();
            personPlay = personPlay.toUpperCase();

            //Print computer's play
            System.out.println("Computer play is = " + computerPlay);

            //See who won Use nested ifs
            if (personPlay.equals(computerPlay)) {
                System.out.println("It's a tie!");
                tie +=1;
            } else if (personPlay.equals("R")) {
                if (computerPlay.equals("S")) {
                    System.out.println("Rock crushes scissors, you win!");
                    win += 1;
                } else if (computerPlay.equals("P")) {
                    System.out.println("Paper eats rock. You lose");
                    lose += 1;
                }
            } else if (personPlay.equals("P")) {
                if (computerPlay.equals("S")) {
                    System.out.println("Scissors cuts Paper. You lose");
                    lose += 1;
                } else if (computerPlay.equals("R")) {
                    System.out.println("Paper eats rock. you win!!");
                    win += 1;
                }
            } else if (personPlay.equals("S")) {
                if (computerPlay.equals("P")) {
                    System.out.println("Scissor cut paper.You win");
                    win += 1;
                } else if (computerPlay.equals("R")) {
                    System.out.println("Rock breaks scissors.You lose");
                    lose += 1;
                }
            }
            System.out.print("Main lagi ? ");
            ulang = scan.next();
        }
        while (ulang.equals("y") || ulang.equals("Y"));
    }
}
