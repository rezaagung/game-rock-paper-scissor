import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class gameRock {
    private JPanel rockPaper;
    private JTextField tfHasil;
    private JButton paperButton;
    private JButton scissorsButton;
    private JButton rockButton;

    public gameRock() {
        Random generator = new Random();
        rockButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer computerInt;
                computerInt = generator.nextInt(3) + 1;
                if(computerInt == 1){
                    tfHasil.setText("Tie !!");
                }
                else if(computerInt == 2){
                    tfHasil.setText("You Lose!!,Computer is Paper");
                }
                else if(computerInt == 3){
                    tfHasil.setText("You Win!!,Computer is Scissors");
                }

            }
        });
        paperButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer computerInt;
                computerInt = generator.nextInt(3) + 1;
                if(computerInt == 1){
                    tfHasil.setText("You Win !!,Computer is Rock");
                }
                else if(computerInt == 2){
                    tfHasil.setText("Tie!!");
                }
                else if(computerInt == 3){
                    tfHasil.setText("You Lose, Computer is Scissors");
                }

            }
        });
        scissorsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Integer computerInt;
                computerInt = generator.nextInt(3) + 1;
                if(computerInt == 1){
                    tfHasil.setText("You Lose !!, Computer is Rock");
                }
                else if(computerInt == 2){
                    tfHasil.setText("You Win!!, Computer is Paper");
                }
                else if(computerInt == 3){
                    tfHasil.setText("Tie");
                }

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("gameRock");
        frame.setContentPane(new gameRock().rockPaper);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
